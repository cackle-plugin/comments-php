<?php
class CackleActivation{
    public static function check($activation_fields){
        $cackle_api = new CackleApi();
        $k_req = $cackle_api->key_validate(
            $activation_fields["siteId"],
            $activation_fields["siteApiKey"],
            $activation_fields["accountApiKey"]);
        if($k_req==NULL) return Array('connected' => false);
        $k_req = json_decode( $k_req, true);
        $k_req = $k_req["siteInfo"];
        if ($k_req['correctKey'] == "true") {
            $cackle_api->cackle_set_param("cackle_apiId",$activation_fields["siteId"]);
            $cackle_api->cackle_set_param("cackle_siteApiKey",$activation_fields["siteApiKey"]);
            $cackle_api->cackle_set_param("cackle_accountApiKey",$activation_fields["accountApiKey"]);
            $cackle_api->cackle_set_param("cackle_lang",$k_req["lang"]);
            $cackle_api->cackle_set_param("cackle_paidsso",$k_req["sso"]);

            $cackle_api->cackle_set_param("cackle_whitelabel",(($k_req["whitelabel"])? 1 : 0));
            $cackle_api->cackle_set_param("cackle_sso",(($activation_fields["sso"])? 1 : 0));
            $cackle_api->cackle_set_param("cackle_sync",(($activation_fields["sync"])? 1 : 0));
            $cackle_api->cackle_set_param("cackle_correctKey",1);

            $arr[]=Array(
                'whitelabel' => $k_req["whitelabel"],
                'lang' => $k_req["lang"],
                'sso' => $k_req["sso"],
                'correctKey' => $k_req['correctKey']

            );
            return $arr;
        }
        else{
            $cackle_api->cackle_set_param("cackle_correctKey",0);
            $arr[]=Array('correctKey' => false);
            return $arr;
        }


    }
}

?>